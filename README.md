## Project Bookworms #
---
[TOC]
---
Tinder like app.

ios en android: native app.

###Concept
4 pagina's
- Grid Geinspireerd voor ontdekken
- Profielpagina
- Wishlist/Favorieten

###Profiel aanmaken
- gebruik maken

###Bibliotheeken
- Alle boeken, DVD, CD

###Eerste keer inlogen
- Data invullen
- Settingsknop - Profiel bewerken.

###Pagina ontdekken
- Pagina met grid om verlanglijst aan te maken en al gelezen (toevoegen aan favorieten)
- A-Z || Recent || Voorgestelde keuze
- Voorgestelde keuze op basis van genres en auteurs en uitleengeschiedenis

###Pagina Profiel
- Gegevens van MijnBib
- Lijst van favoriete boeken
- Recente foto
- Leeftijd
- Seksuele voorkeur
- Vermelding status (single, relatie etc.)
- Op zoek naar: vrienden...

###Pagina Matchen
- Matchen verwijderen
- Toevoegen aan vriendenlijst
- Match liken

###Open Data info
- Zoeken.bibliotheek.be heeft Open Vlacc als belangrijkste gegevensbron. Via de Open Vlacc-collectiebeschrijvingen wordt het bezit van 6 grote openbare bibliotheken getoond: Antwerpen, Brugge, Brussel, Gent, Hasselt en Leuven.
- Zoeken.bibliotheek.be bevat verrijkende informatie als covers, flapteksten en recensies van Leeswolf en Leeswelp.
- Zoeken.bibliotheek bevat ook digitale bronnen zoals de persmappen van Mediargus, de Digitale Bibliotheek voor de Nederlandse Letteren en DigiLeen.
- Zoeken.bibliotheek.be biedt gecontextualiseerde links aan naar Meer over media, Knack Boekenburen, LibraryThing, Google Books, Spotify, iTunes en Wikipedia

Met 'Mijn Bibliotheek'...

- bied je een eenvoudig en gebruiksvriendelijk systeem aan.
- geef je de kans aan je leden om verschillende bibliotheeklidmaatschappen onder één profiel te beheren (1 op 3 leden is namelijk lid van meerdere bibliotheken).
- kunnen je leden alle lidmaatschappen van zijn of haar gezin (bijvoorbeeld het lidmaatschap van de kinderen) vanuit dit profiel beheren.
- verzeker je je van een systeem dat in de toekomst aan relevantie zal winnen. Het zal noodzakelijk zijn
- indien je een aantal digitale diensten comfortabel wil aanbieden zoals bijvoorbeeld thuisgebruik van het Gopress Krantenarchief, E-boeken en Fundels.